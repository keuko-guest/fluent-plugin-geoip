# -*- encoding: utf-8 -*-
# stub: geoip-c 0.9.1 ruby lib
# stub: ext/geoip/extconf.rb

Gem::Specification.new do |s|
  s.name = "geoip-c".freeze
  s.version = "0.9.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Ryan Dahl".freeze, "Matt Todd".freeze, "Charles Brian Quinn".freeze, "Michael Sheakoski".freeze, "Silvio Quadri".freeze, "Andy Lindeman".freeze]
  s.date = "2013-10-14"
  s.description = "Generic GeoIP lookup tool. Based on the geoip_city RubyGem by Ryan Dahl".freeze
  s.email = ["andy@andylindeman.com".freeze, "mtodd@highgroove.com".freeze]
  s.extensions = ["ext/geoip/extconf.rb".freeze]
  s.files = ["ext/geoip/extconf.rb".freeze]
  s.homepage = "http://github.com/mtodd/geoip".freeze
  s.licenses = ["WTFPL".freeze]
  s.rubygems_version = "2.6.14".freeze
  s.summary = "A Binding to the GeoIP C library".freeze

  s.installed_by_version = "2.6.14" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<minitest>.freeze, ["~> 5.0"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_development_dependency(%q<rdoc>.freeze, ["~> 4.0"])
      s.add_development_dependency(%q<rake-compiler>.freeze, ["~> 0.9.1"])
    else
      s.add_dependency(%q<minitest>.freeze, ["~> 5.0"])
      s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_dependency(%q<rdoc>.freeze, ["~> 4.0"])
      s.add_dependency(%q<rake-compiler>.freeze, ["~> 0.9.1"])
    end
  else
    s.add_dependency(%q<minitest>.freeze, ["~> 5.0"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_dependency(%q<rdoc>.freeze, ["~> 4.0"])
    s.add_dependency(%q<rake-compiler>.freeze, ["~> 0.9.1"])
  end
end

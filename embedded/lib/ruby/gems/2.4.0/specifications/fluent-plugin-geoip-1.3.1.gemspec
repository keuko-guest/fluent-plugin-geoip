# -*- encoding: utf-8 -*-
# stub: fluent-plugin-geoip 1.3.1 ruby lib

Gem::Specification.new do |s|
  s.name = "fluent-plugin-geoip".freeze
  s.version = "1.3.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Kentaro Yoshida".freeze]
  s.date = "2019-02-01"
  s.email = ["y.ken.studio@gmail.com".freeze]
  s.homepage = "https://github.com/y-ken/fluent-plugin-geoip".freeze
  s.licenses = ["Apache-2.0".freeze]
  s.rubygems_version = "2.6.14".freeze
  s.summary = "Fluentd Filter plugin to add information about geographical location of IP addresses with Maxmind GeoIP databases.".freeze

  s.installed_by_version = "2.6.14" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, [">= 0"])
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
      s.add_development_dependency(%q<appraisal>.freeze, [">= 0"])
      s.add_development_dependency(%q<test-unit>.freeze, [">= 3.1.0"])
      s.add_development_dependency(%q<test-unit-rr>.freeze, [">= 0"])
      s.add_development_dependency(%q<geoip2_compat>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<fluentd>.freeze, ["< 2", ">= 0.14.8"])
      s.add_runtime_dependency(%q<geoip-c>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<geoip2_c>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<dig_rb>.freeze, [">= 0"])
    else
      s.add_dependency(%q<bundler>.freeze, [">= 0"])
      s.add_dependency(%q<rake>.freeze, [">= 0"])
      s.add_dependency(%q<appraisal>.freeze, [">= 0"])
      s.add_dependency(%q<test-unit>.freeze, [">= 3.1.0"])
      s.add_dependency(%q<test-unit-rr>.freeze, [">= 0"])
      s.add_dependency(%q<geoip2_compat>.freeze, [">= 0"])
      s.add_dependency(%q<fluentd>.freeze, ["< 2", ">= 0.14.8"])
      s.add_dependency(%q<geoip-c>.freeze, [">= 0"])
      s.add_dependency(%q<geoip2_c>.freeze, [">= 0"])
      s.add_dependency(%q<dig_rb>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, [">= 0"])
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<appraisal>.freeze, [">= 0"])
    s.add_dependency(%q<test-unit>.freeze, [">= 3.1.0"])
    s.add_dependency(%q<test-unit-rr>.freeze, [">= 0"])
    s.add_dependency(%q<geoip2_compat>.freeze, [">= 0"])
    s.add_dependency(%q<fluentd>.freeze, ["< 2", ">= 0.14.8"])
    s.add_dependency(%q<geoip-c>.freeze, [">= 0"])
    s.add_dependency(%q<geoip2_c>.freeze, [">= 0"])
    s.add_dependency(%q<dig_rb>.freeze, [">= 0"])
  end
end
